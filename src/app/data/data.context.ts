import { createContext } from "react";
import { ContextProps } from "../@types";

const initial: ContextProps = {
  user: null,
  setState: () => {},
  authenticated: false,
};

export default createContext<Partial<ContextProps>>(initial);
