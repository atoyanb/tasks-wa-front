import Task from "../components/Task";
import { NavLink } from "react-router-dom";
import {
  Box,
  Center,
  Stack,
  Spinner,
  Button,
  Flex,
  Heading,
  useDisclosure,
  useToast,
  IconButton,
  Tooltip,
  Input,
  FormControl,
  Text,
} from "@chakra-ui/react";
import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import Add from "../components/Add";
import { queryClient } from "../..";
import { useQuery } from "react-query";
import { fetchTasks } from "../api/hooks";
import { Task as TaskType } from "../@types";
import dataContext from "../data/data.context";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";

const Home = () => {
  const toast = useToast();
  const [page, setPage] = useState(1);
  const { onOpen, isOpen, onClose } = useDisclosure();
  const { authenticated, setState } = useContext(dataContext);
  const { data: tasks, isFetching } = useQuery(
    ["tasks", page],
    () => fetchTasks(page),
    {
      keepPreviousData: true,
    }
  );

  const handleLogout = () => {
    localStorage.removeItem("token");
    setState!!({ authenticated: false, user: null });
    toast({ title: "Logged out", isClosable: false });
  };

  useEffect(() => {
    queryClient.prefetchQuery(["tasks", page + 1], () => fetchTasks(page + 1));
  }, [tasks, page]);

  return (
    <Box minH={"100vh"}>
      <Box w={"full"} p={5}>
        <Box mt={14}>
          <Center>
            <Box w={["2xl", "4xl"]}>
              <Stack spacing={10}>
                <Flex alignItems={"center"} justifyContent={"space-between"}>
                  <Heading>Tasks</Heading>
                  <Box>
                    <Stack direction={"row"}>
                      <Button onClick={onOpen} colorScheme={"green"}>
                        Add task
                      </Button>
                      <Add onClose={onClose} isOpen={isOpen} />

                      {!authenticated ? (
                        <NavLink to={"/login"}>
                          <Button colorScheme={"blue"}>Login</Button>
                        </NavLink>
                      ) : (
                        <Box>
                          <Button onClick={handleLogout} colorScheme={"red"}>
                            Logout
                          </Button>
                        </Box>
                      )}
                    </Stack>
                  </Box>
                </Flex>

                <Box>
                  <Box p={2} h={"md"}>
                    <Stack spacing={5}>
                      {isFetching ? (
                        <Center minH={"30vh"}>
                          <Spinner />
                        </Center>
                      ) : (
                        <Stack spacing={4}>
                          {tasks?.message?.tasks?.map((task: TaskType) => {
                            return <Task key={task.id} data={task} />;
                          })}
                        </Stack>
                      )}

                      <Box>
                        <Center>
                          <Stack>
                            <Center>
                              <Stack direction={"row"}>
                                <Tooltip
                                  placement={"left"}
                                  label={"Previous page"}
                                >
                                  <IconButton
                                    onClick={() => setPage(page - 1)}
                                    aria-label={"Previous page"}
                                    icon={<FiChevronLeft />}
                                    isDisabled={page === 1}
                                    rounded={"xl"}
                                  />
                                </Tooltip>

                                <Tooltip
                                  placement={"right"}
                                  label={"Next page"}
                                >
                                  <IconButton
                                    rounded={"xl"}
                                    aria-label={"Next page"}
                                    icon={<FiChevronRight />}
                                    isDisabled={
                                      Number(tasks?.message?.total_task_count) /
                                        3 ===
                                      page
                                    }
                                    onClick={() => setPage(page + 1)}
                                  />
                                </Tooltip>
                              </Stack>
                            </Center>

                            <FormControl>
                              <Stack direction={"row"} alignItems={"center"}>
                                <Text>Page</Text>

                                <Input
                                  type={"number"}
                                  onChange={(event) => {
                                    const { value: page } = event.target;

                                    const maxPages =
                                      Number(tasks?.message?.total_task_count) %
                                        3 ===
                                      0
                                        ? Number(
                                            tasks?.message?.total_task_count
                                          ) / 3
                                        : Math.floor(
                                            Number(
                                              Number(
                                                tasks?.message?.total_task_count
                                              ) / 3
                                            ) + 1
                                          );

                                    if (Number(page) > maxPages) {
                                      setPage(maxPages);
                                      event.target.value = maxPages.toString();
                                    } else if (Number(page) < 1) {
                                      setPage(1);
                                      event.target.value = (1).toString();
                                    } else setPage(Number(page));
                                  }}
                                  value={page}
                                  placeholder={"Page number"}
                                />
                              </Stack>
                            </FormControl>
                          </Stack>
                        </Center>
                      </Box>
                    </Stack>
                  </Box>
                </Box>
              </Stack>
            </Box>
          </Center>
        </Box>
      </Box>
    </Box>
  );
};

export default Home;
