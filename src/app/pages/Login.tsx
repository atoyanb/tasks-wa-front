import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Button,
  Heading,
  useColorModeValue,
  Center,
  Link,
  useToast,
} from "@chakra-ui/react";
import axios from "axios";
import dataContext from "../data/data.context";
import { NavLink, Redirect } from "react-router-dom";
import { ChangeEvent, useState, useContext } from "react";

const Login = () => {
  const toast = useToast();
  const { setState } = useContext(dataContext);
  const [submitting, setIsSubmitting] = useState(false);

  const handleLogin = async (event: ChangeEvent<HTMLFormElement>) => {
    setIsSubmitting(true);
    event.preventDefault();

    const { data } = await axios.post(
      "https://uxcandy.com/~shapoval/test-task-backend/v2/login?developer=Name",
      new FormData(event.target)
    );

    setIsSubmitting(false);

    switch (data?.status) {
      case "ok": {
        setState!!({
          authenticated: true,
        });
        localStorage.token = data?.message?.token;
        return <Redirect to={"/"} />;
      }
      default: {
        if (data?.message?.password) {
          return toast({
            title: "Invalid username or password",
            status: "error",
            isClosable: false,
          });
        } else {
          return toast({
            status: "error",
            isClosable: false,
            title: "Invalid credentials",
          });
        }
      }
    }
  };

  return (
    <Flex
      minH={"100vh"}
      align={"center"}
      justify={"center"}
      bg={useColorModeValue("gray.50", "gray.800")}
    >
      <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"}>Sign in to your account 🔒</Heading>
        </Stack>

        <form
          autoComplete={"off"}
          onSubmit={handleLogin}
          encType={"multipart/form-data"}
        >
          <Box
            rounded={"lg"}
            bg={useColorModeValue("white", "gray.700")}
            boxShadow={"lg"}
            p={8}
          >
            <Stack spacing={4}>
              <FormControl>
                <FormLabel>Username</FormLabel>
                <Input
                  required
                  autoFocus
                  type={"text"}
                  name={"username"}
                  placeholder={"Username"}
                />
              </FormControl>
              <FormControl>
                <FormLabel>Password</FormLabel>
                <Input
                  required
                  name={"password"}
                  type={"password"}
                  placeholder={"Password"}
                />
              </FormControl>
              <Stack spacing={10}>
                <Button
                  type={"submit"}
                  bg={"blue.400"}
                  color={"white"}
                  name={"password"}
                  isLoading={submitting}
                  _hover={{
                    bg: "blue.500",
                  }}
                >
                  Sign in
                </Button>

                <Center>
                  <Link as={NavLink} to={"/"} color={"blue.300"}>
                    Back to the main page
                  </Link>
                </Center>
              </Stack>
            </Stack>
          </Box>
        </form>
      </Stack>
    </Flex>
  );
};

export default Login;
