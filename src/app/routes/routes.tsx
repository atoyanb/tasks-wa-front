import { Switch, Route } from "react-router-dom";
import PrivateRoute from "../helpers/Protected";
import Home from "../pages/Home";
import Login from "../pages/Login";

const Routes = () => {
  return (
    <Switch>
      <Route path={"/"} exact component={Home} />
      <PrivateRoute swap>
        <Route path={"/login"} exact component={Login} />
      </PrivateRoute>
    </Switch>
  );
};

export default Routes;
