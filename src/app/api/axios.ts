import axios from "axios";

export default axios.create({
  validateStatus: null,
  baseURL: "https://uxcandy.com/~shapoval/test-task-backend/v2",
});
