import axios from "axios";

export const fetchTasks = async (page: string | number) => {
  const { data } = await axios.get(
    "https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=Name",
    {
      params: {
        page,
      },
    }
  );
  return data;
};
