import React from "react";

export type Task = {
  id: number;
  text: string;
  email: string;
  status: number;
  username: string;
};

export interface ContextProps {
  user: any;
  authenticated: boolean;

  setState: React.Dispatch<
    React.SetStateAction<
      Partial<{
        user: any;
        authenticated: boolean;
      }>
    >
  >;
}
