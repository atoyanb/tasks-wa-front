import Routes from "./routes/routes";
import { BrowserRouter as Router } from "react-router-dom";
import dataContext from "./data/data.context";
import { useState } from "react";
import { useEffect } from "react";

const App = () => {
  const [state, setState] = useState({
    user: null,
    authenticated: false,
  });

  useEffect(() => {
    if (localStorage.token) setState({ authenticated: true });
  }, [setState]);

  return (
    <dataContext.Provider
      value={{
        setState,
        user: state?.user,
        authenticated: state?.authenticated,
      }}
    >
      <Router>
        <Routes />
      </Router>
    </dataContext.Provider>
  );
};
export default App;
