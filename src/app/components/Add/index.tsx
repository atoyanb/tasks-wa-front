import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Box,
  Stack,
  FormControl,
  useToast,
  Textarea,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { useContext } from "react";
import { useState } from "react";
import { ChangeEvent } from "react";
import axios from "../../api/axios";
import dataContext from "../../data/data.context";

interface AddProps {
  onClose: any;
  isOpen: boolean;
}

const Add = ({ isOpen, onClose }: AddProps) => {
  const toast = useToast();
  const { setState } = useContext(dataContext);
  const [isLoading, setIsLoading] = useState(false);

  const handleAddTask = async (event: ChangeEvent<HTMLFormElement>) => {
    setIsLoading(true);
    event.preventDefault();

    const { data } = await axios.post(
      "https://uxcandy.com/~shapoval/test-task-backend/v2/create/?developer=Name",
      new FormData(event.target)
    );

    setIsLoading(false);

    switch (data.status) {
      case "ok": {
        return toast({
          title: "Task added!",
          status: "success",
          isClosable: false,
        });
      }
      default: {
        if (data?.error?.message?.token) {
          toast({
            title: "Your token has expired, please login again",
            status: "error",
            isClosable: false,
          });
          return setState!!({ authenticated: false, user: null });
        } else {
          return toast({
            title: "There was an error",
            status: "error",
            isClosable: false,
          });
        }
      }
    }
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <form onSubmit={handleAddTask} encType={"multipart/form-data"}>
          <ModalHeader>Add a task</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Box>
              <Stack spacing={3}>
                <FormControl isRequired>
                  <FormLabel>Username</FormLabel>
                  <Input
                    required
                    type={"text"}
                    name={"username"}
                    placeholder={"John Doe 123"}
                  />
                </FormControl>

                <FormControl isRequired>
                  <FormLabel>Email</FormLabel>
                  <Input
                    required
                    name={"email"}
                    type={"email"}
                    placeholder={"john@doe.com"}
                  />
                </FormControl>

                <FormControl isRequired>
                  <FormLabel>Task body</FormLabel>
                  <Textarea
                    required
                    name={"text"}
                    type={"text"}
                    placeholder={"Hello world...."}
                  />
                </FormControl>
              </Stack>
            </Box>
          </ModalBody>

          <ModalFooter>
            <Button
              w={"full"}
              type={"submit"}
              colorScheme={"green"}
              isLoading={isLoading}
            >
              Save
            </Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

export default Add;
