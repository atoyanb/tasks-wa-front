import {
  Box,
  Text,
  Checkbox,
  Flex,
  Spacer,
  Badge,
  chakra,
  Stack,
  Editable,
  EditablePreview,
  EditableInput,
  useToast,
} from "@chakra-ui/react";
import axios from "axios";
import { Task as TaskType } from "../../@types";
import dataContext from "../../data/data.context";
import { ChangeEvent, useContext, useState } from "react";

interface TaskProps {
  data: TaskType;
}

const Task = ({ data }: TaskProps) => {
  const toast = useToast();
  const [task, setState] = useState<Partial<TaskType>>(data);
  const { authenticated, setState: setContextState } = useContext(dataContext);

  const handleCheckboxChange = async (_: ChangeEvent<HTMLInputElement>) => {
    let updated: Number;
    const formData = new FormData();

    if (task.status === 0) updated = 10;
    else if (task.status === 1) updated = 11;
    else if (task.status === 10) updated = 0;
    else if (task.status === 11) updated = 1;

    formData.append("token", localStorage.token);
    formData.append("status", String(updated!!));

    const { data } = await axios.post(
      `https://uxcandy.com/~shapoval/test-task-backend/v2/edit/${task.id}/?developer=Name`,
      formData
    );

    if (data?.status === "error") {
      if (data?.message?.token) {
        localStorage.removeItem("token");
        setContextState!!({ authenticated: false, user: null });
      } else {
        toast({
          title: "Unexpected error",
          isClosable: false,
          status: "error",
        });
      }
    } else setState({ ...task, status: Number(updated!!) });
  };

  const handleTextChange = async (value: string) => {
    const formData = new FormData();
    formData.append("text", value);
    formData.append("token", localStorage.token);

    const { data } = await axios.post(
      `https://uxcandy.com/~shapoval/test-task-backend/v2/edit/${task.id}/?developer=Name`,
      formData
    );

    if (!data?.error) setState((old) => ({ ...old, text: value }));
    else {
      if (data?.message?.token && data?.status === "error") {
        localStorage.removeItem("token");
        setContextState!!({ authenticated: false, user: null });
      } else
        toast({
          title: "Unexpected error",
          isClosable: false,
          status: "error",
        });
    }
  };

  return (
    <Box
      py={3}
      px={5}
      rounded={"lg"}
      border={"2px"}
      transition={"all 150ms"}
      _hover={{
        opacity: "1",
        boxShadow: "xl",
        bgColor:
          task?.status === 10 || task?.status === 11
            ? "green.100 "
            : "gray.100",
        borderColor:
          task?.status === 10 || task?.status === 11 ? "green.200" : "gray.400",
      }}
      bgColor={task.status === 10 || task.status === 11 ? "green.50" : ""}
      borderColor={
        task.status === 10 || task.status === 11 ? "green.200" : "gray.400"
      }
      opacity={task.status === 10 || task.status === 11 ? "0.5" : "initial"}
    >
      <Stack spacing={3}>
        <Stack direction={"row"}>
          {task?.status === 10 || task?.status === 11 ? (
            <Badge fontSize={"0.9rem"} colorScheme={"green"}>
              Completed
            </Badge>
          ) : null}

          {task?.status === 11 || task?.status === 1 ? (
            <Badge fontSize={"0.9rem"} colorScheme={"orange"}>
              Edited by admin
            </Badge>
          ) : null}
        </Stack>

        <Flex>
          <Stack userSelect={"none"} spacing={1}>
            <Text fontWeight={"semibold"}>
              {task?.username} •{" "}
              <chakra.span textDecoration={"underline"} color={"gray.500"}>
                {task?.email}
              </chakra.span>
            </Text>

            {authenticated ? (
              <Editable onSubmit={handleTextChange} defaultValue={task?.text}>
                <EditablePreview
                  fontSize={"2xl"}
                  fontWeight={"bold"}
                  textDecoration={
                    task?.status === 10 || task?.status === 11
                      ? "line-through"
                      : "none"
                  }
                  color={
                    task?.status === 10 || task?.status === 11
                      ? "green.200"
                      : "gray.700"
                  }
                />
                <EditableInput fontSize={"2xl"} fontWeight={"bold"} />
              </Editable>
            ) : (
              <Text
                fontSize={"2xl"}
                fontWeight={"bold"}
                textDecoration={
                  task?.status === 10 || task?.status === 11
                    ? "line-through"
                    : "none"
                }
                color={
                  task?.status === 10 || task?.status === 11
                    ? "green.200"
                    : "gray.700"
                }
              >
                {task?.text}
              </Text>
            )}
          </Stack>

          <Spacer />

          {authenticated && (
            <Stack spacing={5} alignItems={"center"} direction={"row"}>
              <Checkbox
                size={"lg"}
                onChange={handleCheckboxChange}
                isChecked={task?.status === 10 || task?.status === 11}
                colorScheme={
                  task?.status === 10 || task?.status === 11 ? "green" : "red"
                }
              />
            </Stack>
          )}
        </Flex>
      </Stack>
    </Box>
  );
};

export default Task;
